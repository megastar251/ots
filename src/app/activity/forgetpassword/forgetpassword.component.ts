import { ApiCommunicateService } from './../../service/api-communicate/api-communicate.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {
  statusdata: any;
  email: any;
  code: any;

  constructor(private route: ActivatedRoute, private myconnection: ApiCommunicateService) { }

  ngOnInit() {
    this.route.queryParams
      .filter(params => params.email)
      .subscribe(params => {
        this.email = params.email;
        this.code = params.code;
      });
  }

  verifyUser(f) {
    this.myconnection
      .postData('passwordresetsubmit', {
        email: this.email,
        code: this.code,
        password: f.value.password1,
        c_password: f.value.password2,
      })
      .subscribe(result => {
        this.statusdata = result.data;
      });
  }

  // skillFormSubmit(myform) {

  //   console.log(myform.value);
  //   const formdata = myform.value;
  //   this.myconnection
  //     .postData('skill', {
  //       user: this.userinfo.getUserName(),
  //       data: formdata,
  //     })
  //     .subscribe(result => {
  //       this.skilldata = result.data;
  //     });
  // }

}
