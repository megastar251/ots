import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { VerifyemailComponent } from './verifyemail/verifyemail.component';
import { ActivitydashboardComponent } from './activitydashboard/activitydashboard.component';
import { QuerryComponent } from './querry/querry.component';
import { ForgetpasswordformComponent } from './forgetpasswordform/forgetpasswordform.component';

const routes: Routes = [
  {
    path: 'passwordreset',
    component: ForgetpasswordComponent,
  },
  {
    path: 'passwordresetrequest',
    component: ForgetpasswordformComponent,
  },
  {
    path: 'querry',
    component: QuerryComponent,
  },
  {
    path: 'verifyemail',
    component: VerifyemailComponent,
  },
  {
    path: '',
    component: ActivitydashboardComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
  ForgetpasswordComponent,
  VerifyemailComponent,
  ActivitydashboardComponent,
  QuerryComponent,
  ForgetpasswordformComponent]
})
export class ActivityModule { }
