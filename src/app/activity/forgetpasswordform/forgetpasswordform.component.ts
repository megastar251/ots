import { ApiCommunicateService } from './../../service/api-communicate/api-communicate.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgetpasswordform',
  templateUrl: './forgetpasswordform.component.html',
  styleUrls: ['./forgetpasswordform.component.css']
})
export class ForgetpasswordformComponent implements OnInit {
  statusdata: any;
  showbutton: boolean;
  showvalidating: boolean;
  showvalidatingmsg: any;

  constructor(private route: ActivatedRoute, private myconnection: ApiCommunicateService) { }

  ngOnInit() {
    this.showbutton = true;
    this.showvalidating = false;
    this.showvalidatingmsg = 'Please wait while we Validates your Email ID';
  }

  verifyUser(f) {
    this.showbutton = false;
    this.showvalidating = true;
    this.myconnection
      .postData('passwordreset', {
        email: f.value.email,
      })
      .subscribe(result => {
        this.statusdata = result.data;
        this.showvalidating = false;
      });
  }

}
