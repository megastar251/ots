import { ApiCommunicateService } from './../../service/api-communicate/api-communicate.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verifyemail',
  templateUrl: './verifyemail.component.html',
  styleUrls: ['./verifyemail.component.css']
})
export class VerifyemailComponent implements OnInit {
  statusdata: any;
  email: any;
  code: any;

  constructor(private route: ActivatedRoute, private myconnection: ApiCommunicateService) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.email = params.email;
        this.code = params.code;

      });
    this.verifyUser();
  }

  verifyUser() {
    this.myconnection
      .postData('verifyemail', {
        email: this.email,
        code: this.code,
      })
      .subscribe(result => {
        this.statusdata = result.data;
      });
  }

}
