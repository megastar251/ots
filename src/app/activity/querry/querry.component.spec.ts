import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuerryComponent } from './querry.component';

describe('QuerryComponent', () => {
  let component: QuerryComponent;
  let fixture: ComponentFixture<QuerryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuerryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuerryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
