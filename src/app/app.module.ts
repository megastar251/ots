import { GlobalEmitterService } from './service/global-emitter/global-emitter.service';

// services directly attached
import { ApiCommunicateService } from './service/api-communicate/api-communicate.service';
import { UserDataService } from './service/user-data/user-data.service';
import { CookieService } from 'ngx-cookie-service';


// required by angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app.routing';
// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// components directly attached
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { LinkNotFoundComponent } from './link-not-found/link-not-found.component';

// chart ----- to be removed later
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import {NgxChartsModule} from '@swimlane/ngx-charts';


// pipe that are required directly
import { SafeHtmlPipe } from './safe-html.pipe';

// guards attached directly
import { OnlyLoggedInUsersGuard } from './guard/only-logged-in-users/only-logged-in-users.guard';
import { LoadingbarComponent } from './loadingbar/loadingbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    FooterComponent,
    HeaderComponent,
    SafeHtmlPipe,
    LinkNotFoundComponent,
    LoadingbarComponent,
  ],
  imports: [
    BsDropdownModule.forRoot(),
    BrowserModule,
    RouterModule,
    ReactiveFormsModule,
    // BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    // NgxChartsModule,
    // NgbModule.forRoot()
  ],

  providers: [UserDataService,
    ApiCommunicateService,
    CookieService,
    OnlyLoggedInUsersGuard,
    GlobalEmitterService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
