import { TestBed, inject } from '@angular/core/testing';

import { ApiCommunicateService } from './api-communicate.service';

describe('ApiCommunicateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiCommunicateService]
    });
  });

  it('should be created', inject([ApiCommunicateService], (service: ApiCommunicateService) => {
    expect(service).toBeTruthy();
  }));
});
