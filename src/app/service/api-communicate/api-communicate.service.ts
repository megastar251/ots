import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/retryWhen';
// import { Observable } from 'rxjs/Rx';
import { UserDataService } from '../user-data/user-data.service';

@Injectable()
export class ApiCommunicateService {
  options: RequestOptions;
  // apiurl = 'http://localhost:80/home/adesh/Documents/blog/public/api/v1/';
  apiurl = 'http://www.app.opentestseries.com/public/api/v1/';
  // apiurl = 'http://www.manage.opentestseries.com/public/api/v1/';
  // apiurl:string = 'https://manage.opentestseries.com/public/api/v1';
  url: any;

  constructor(private http: Http, private userinfo: UserDataService, ) { }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
  }

  setGuestWelcome() {
  }

  postData(requesturl: string, content: any) {
    // tslint:disable-next-line:prefer-const
    let recieveddata: any;
    if (this.userinfo.getLoggedInStatus()) {
      //   headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.getAuthAccessToken())
      const h = 'Bearer ' + this.userinfo.getUserToken();
      this.options = new RequestOptions({
        headers: new Headers({
          // 'Content-Type': 'multipart/form-data',
          'Authorization': h,
        })
      });

      this.url = this.apiurl + requesturl;
      return this.http.post(this.url, content, this.options)
        .map((res) => res.json())
        .retry(5);

    } else {
      //
      this.url = this.apiurl + requesturl;
      return this.http.post(this.url, content)
        .map((res) => res.json())
        .retry(5);
    }
  }

  getData(requesturl: string) {
    const h = 'Bearer ' + this.userinfo.getUserToken();
    this.options = new RequestOptions({
      headers: new Headers({
        // 'Content-Type': 'multipart/form-data',
        'Authorization': h,
      })
    });

      this.url = this.apiurl + requesturl;
      return this.http.get(this.url, this.options)
                .map((res) => res.json())
                .retry(10);
  }

  postImportantData(requesturl: string, content: any) {
    // tslint:disable-next-line:prefer-const
    let recieveddata: any;
    if (this.userinfo.getLoggedInStatus()) {
      const h = 'Bearer ' + this.userinfo.getUserToken();
      this.options = new RequestOptions({ headers: new Headers({ 'Authorization': h, }) });

      this.url = this.apiurl + requesturl;
      return this.http.post(this.url, content, this.options)
        .map((res) => res.json())
        .retry();

    } else {
      //
      this.url = this.apiurl + requesturl;
      return this.http.post(this.url, content)
        .map((res) => res.json())
        .retry();
    }
  }


  getImportantData(requesturl: string) {
    const h = 'Bearer ' + this.userinfo.getUserToken();
    this.options = new RequestOptions({
      headers: new Headers({
        'Authorization': h,
      })
    });

    const randomtime = Math.floor((Math.random() * 25000) + 5000);
    this.url = this.apiurl + requesturl;
    return this.http.get(this.url, this.options)
      .map((res) => res.json())
      .retryWhen(errors => errors.delay(randomtime).take(10));
  }


  uploadImageData(requesturl: string, content: any) {
    this.url = this.apiurl + requesturl;
    const x = 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2);
    this.options = new RequestOptions({
      headers: new Headers({

        'Content-type': x,
      })
    });
    return this.http.post(this.url, content, this.options)
      .map((res) => res.json());
      // .retry(3);
  }

  upload(formData, options, id?, requesturl?) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.delete('Content-Type');
    options = new RequestOptions({ headers: headers });
    this.url = this.apiurl + requesturl;
  }
}
