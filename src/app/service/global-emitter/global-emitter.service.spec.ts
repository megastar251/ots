import { TestBed, inject } from '@angular/core/testing';

import { GlobalEmitterService } from './global-emitter.service';

describe('GlobalEmitterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalEmitterService]
    });
  });

  it('should be created', inject([GlobalEmitterService], (service: GlobalEmitterService) => {
    expect(service).toBeTruthy();
  }));
});
