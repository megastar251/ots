import { Injectable, EventEmitter, Renderer } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Injectable()
export class GlobalEmitterService {
  public showNavBar: EventEmitter<any> = new EventEmitter();

  constructor(private meta: Meta, private titleService: Title, ) {}

  ngOninit() {
    //
  }

  renderTitle(recievedtitle: any) {
    this.titleService.setTitle(recievedtitle);
  }
  renderMeta(recievedMetaData: any) {
    for (const key in recievedMetaData) {
      if (recievedMetaData.hasOwnProperty(key)) {
        const element = recievedMetaData[key];
        console.log(element.metaname, element.metavalue);
        switch (element.metatype) {
          case 'metaname':
            this.meta.removeTag('name='+ element.metaname);
            this.meta.updateTag({ name: element.metaname, content: element.metavalue });
            break;
          case 'metaitemprop':
            this.meta.removeTag('itemprop='+ element.metaname);       
            this.meta.updateTag({ itemprop: element.metaname, content: element.metavalue });
            break;
          case 'metaproperty':
            this.renderfacebooktags(element.metaname);
            this.meta.updateTag({ property: element.metaname, content: element.metavalue });
            break;
          default:
            break;
        }
      }
    }
  }

  renderfacebooktags(name: any) {
    switch (name) {
      case 'og:title':
        this.meta.removeTag('property="og:title"');
      break;
      case 'og:description':
        this.meta.removeTag('property="og:description"');
      break;

      case 'og:url':
        this.meta.removeTag('property="og:url"');
      break;
      case 'og:image':
        this.meta.removeTag('property="og:image"');
      break;
      default:
      break;
    }
  }
}
