import { TestBed, inject } from '@angular/core/testing';

import { HomePageNotifService } from './home-page-notif.service';

describe('HomePageNotifService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HomePageNotifService]
    });
  });

  it('should be created', inject([HomePageNotifService], (service: HomePageNotifService) => {
    expect(service).toBeTruthy();
  }));
});
