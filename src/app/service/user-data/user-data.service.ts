
import {
  Injectable, EventEmitter, OnInit, OnDestroy } from '@angular/core';

@Injectable()
export class UserDataService {
  lang: any;
  private username = 'adesh';
  private email: any;
  private courses: any;
  private currentcourse: any;
  private tokenKey = 'app_token';
  private currenturllocation: string;

  // myMenuStatusEmmiter: EventEmitter<boolean> = new EventEmitter<boolean>();
  myMenuStatusEmmiter: EventEmitter<String> = new EventEmitter<String>();
  public EmittedLogedInStatus: EventEmitter<boolean> = new EventEmitter();
  public EmittedLogedInUserName: EventEmitter<any> = new EventEmitter();
  public showNavBarUserName: EventEmitter<any> = new EventEmitter();
  public EmittedAlertStatus: EventEmitter<boolean> = new EventEmitter();
  public EmittedAlertMessage: EventEmitter<any> = new EventEmitter();

  // setheaderMenu($content: boolean) {
  //   this.header = $content;
  // }
  // getheaderMenu() {
  //   return this.header;
  // }

  /*
  *------------------------------------------------------------
  *                  For GUEST USER
  *-----------------------------------------------------------
  */
  private guestuserid: any;

  /*
  *------------------------------------------------------------
  *                  For Header Menu bar
  *-----------------------------------------------------------
  */
  private header: any;
  private AlertMessageinHeaderContent: String;
  private AlertMessageinHeaderStatus: boolean;

  constructor() {
    // if (this.getLoggedInStatus()) {
    //   this.emittusername(this.getUserName());
    // }
    // this.emittUserLogedinStatus(false);

  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {

    if (this.getLoggedInStatus()) {
      this.emittusername(this.getUserName());
    }
    this.emittUserLogedinStatus(false);
    // this.showNavBarUserName.emit(this.getUserName());
    this.header = false;
    this.AlertMessageinHeaderStatus = false;
    this.AlertMessageinHeaderContent = 'New Practice test is avaliable! have a look on it.';
  }





  // clear all the data when logged out
  // in future i will set guest immediately when logged out
  clearAll() {
    this.username = '';
    this.email = null;
    this.courses = null;
    this.currentcourse = null;

    this.clearToken();
    this.clearUserName();
  }








  // ******* SETTING USER's NAME  **********
  setUserName(content: string) {
    localStorage.setItem('username', JSON.stringify(content));
    this.showNavBarUserName.emit(content);
    // this.EmittedLogedInUserName.emit(content);
    this.emittusername(content);
    this.username = content;
  }
  getUserName() {
    let usr: string;
    try {
      usr = localStorage.getItem('username');
    } catch (err) {
      return 'Guest ';
    }
    return usr + '';
  }
  clearUserName() {
    localStorage.removeItem('username');
    this.emittusername('');
  }








  // ************ SETTING USER's EMAIL ID ************
  setEmail(content: string) {
    this.email = content;
  }
  getEmail() {
    return this.email;
  }









  // ************* SETTING USER's TOKEN *****************
  setUserToken(content: string) {
    const y = JSON.stringify(content);
    localStorage.setItem(this.tokenKey, y.substring(1, y.length - 1) );
  }
  getUserToken() {
    let storedToken: string;
    try {
      storedToken = localStorage.getItem(this.tokenKey);
    } catch (err) {
      return '';
    }
    return storedToken;
  }
  clearToken() {
    localStorage.removeItem(this.tokenKey);
  }








// ################# CHECKING USERS LOGIN STATUS ##################
  getLoggedInStatus() {
    if (this.getUserToken() === '') {
      return false;
    } else if (this.getUserToken() == null) {
      return false;
    } else {
      return true;
    }
  }


  // ++++++++++++++++++++++++
  // Emitters
  emittusername(content) {
    // this.EmittedLogedInUserName.emit(content);
    if (content === '') {
      this.EmittedLogedInUserName.emit(content);
      this.emittUserLogedinStatus(false);
    } else {
      this.EmittedLogedInUserName.emit(content);
      this.emittUserLogedinStatus(true);
    }
  }
  emittUserLogedinStatus(content) {
    this.EmittedLogedInStatus.emit(content);
  }

  emittAlertStatus(content) {
    this.EmittedLogedInStatus.emit(content);
  }
  emittAlertMessage(content) {
    if (content === '') {
      this.EmittedAlertMessage.emit(content);
      this.emittAlertStatus(false);
    } else {
      this.EmittedAlertMessage.emit(content);
      this.emittAlertStatus(true);
    }
  }






  /*
  *------------------------------------------------------------
  *                  For GUEST USER
  *-----------------------------------------------------------
  */

  setGuestid($content) {
    this.guestuserid = $content;
    this.showNavBarUserName.emit('Guest');
  }
  getGuestid() {
    return this.guestuserid;
  }

  /*
  *------------------------------------------------------------
  *                  For Header Setting
  *-----------------------------------------------------------
  */
  setheaderMenu(lang) {
    this.AlertMessageinHeaderContent = lang;
    this.myMenuStatusEmmiter.emit(this.AlertMessageinHeaderContent);
  }

  getheaderMenu() {
    return this.AlertMessageinHeaderContent;
  }
  setheaderAlertMessage($content: String) {
    this.AlertMessageinHeaderContent = $content;
  }
  getheaderAlertMessage(c) {
    return this.AlertMessageinHeaderContent;
  }
  setheaderAlertMessageStatus($content: boolean) {
    this.AlertMessageinHeaderStatus = $content;
  }
  getheaderAlertMessageStatus() {
    return this.AlertMessageinHeaderStatus;
  }
}
