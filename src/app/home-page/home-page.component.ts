import { UserDataService } from './../service/user-data/user-data.service';
import { CookieService } from 'ngx-cookie-service';
import { ApiCommunicateService } from './../service/api-communicate/api-communicate.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  topRanker: any;
  subscribemsg: any;
  toptestdata: any;
  showSpinner = true;
  subsc: any;
  notifGroupsData: any;
  private cookieExists: boolean;
  marquee1Data: any;
  marquee2Data: any;
  trending1Data: any;
  followUsData: any;

  // navigation link data
  latestJobs: any;
  examSyllabus: any;
  examResults: any;
  admission: any;
  formsEndingSoon: any;
  examInformation: any;
  practiceTest: any;
  studyResources: any;
  trendings: any;
  newsAndNotification: any;
  importantWebsites: any;
  challenges: any;

  loadingbarChild = true;
  constructor(
    private myconnection: ApiCommunicateService,
    private cookieService: CookieService,
    private userinfo: UserDataService
  ) {}

  ngOnInit() {
    // getting data from server
    this.getMarquees();
    this.getNotifgroups();
    this.getAvaliableTopTestData();
  }

  getMarquees() {
    try {
      this.subsc = this.myconnection
        .getData('notificationMarque1')
        .subscribe(result => {
          this.marquee1Data = result.data;
        });

      this.subsc = this.myconnection
        .getData('notificationMarque2')
        .subscribe(result => {
          this.marquee2Data = result.data;
        });
    } catch (error) {}
  }

  getNotifgroups() {
    try {
      this.subsc = this.myconnection
        .getData('notificationgroups')
        .subscribe(result => {
          this.latestJobs = result.latestjobs;
          this.examSyllabus = result.examsyllabus;
          this.examResults = result.examresults;
          this.admission = result.admission;
          this.formsEndingSoon = result.formsendingsoon;
          this.examInformation = result.examinformation;
          this.practiceTest = result.practicetest;
          this.studyResources = result.studyresources;
          this.trendings = result.trendings;
          this.newsAndNotification = result.newsandnotification;
          this.importantWebsites = result.importantwebsites;
          this.challenges = result.challenges;
          this.topRanker = result.topRanker;
          this.loadingbarChild = false;
        });
    } catch (error) {}
  }

  getAvaliableTopTestData() {
    this.myconnection
      .getData('toptestlist')
      .subscribe(result => {
        this.toptestdata = result.data;
      });
  }

  newSubscribe(f) {

    const formdata = f.value;
    this.myconnection
      .postData('subscribe', {
        data: formdata,
      })
      .subscribe(result => {
        this.subscribemsg = result.data;
      });

  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    try {
      this.subsc.unsubscribe();
    } catch (error) {}
  }
}
