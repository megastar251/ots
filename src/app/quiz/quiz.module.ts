import { TestResultComponent } from './test-result/test-result.component';
import { TestCourseComponent } from './test-course/test-course.component';
import { TestTopicComponent } from './test-topic/test-topic.component';
import { TestListComponent } from './test-list/test-list.component';
import { TestStartComponent } from './test-start/test-start.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MakeSafeHtmlPipe } from './make-safe-html.pipe';
import { OnlyLoggedInUsersGuard } from '../guard/only-logged-in-users/only-logged-in-users.guard';
import { SharedModuleModule } from '../shared-module/shared-module.module';

const routes: Routes = [
  {
    path: 'result',  // it will generate courses avaliable
    component: TestResultComponent,
    // canActivate: [OnlyLoggedInUsersGuard],
  },
  {
    path: ':course_slug/:topic_slug/:list_slug', // it will fatch data and start test
    component: TestStartComponent,
    canActivate: [OnlyLoggedInUsersGuard],
  },
  {
    path: ':course_slug/:topic_slug',  // it will generate list of selected
    component: TestListComponent,
    // canActivate: [OnlyLoggedInUsersGuard],
  },
  {
    path: ':course_slug', // it will generate topic of selected
    component: TestTopicComponent,
    // canActivate: [OnlyLoggedInUsersGuard],
  },
  {
    path: '',  // it will generate courses avaliable
    component: TestCourseComponent,
    // canActivate: [OnlyLoggedInUsersGuard],
  },
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModuleModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    TestStartComponent,
    TestListComponent,
    TestTopicComponent,
    TestCourseComponent,
    TestResultComponent,
    MakeSafeHtmlPipe
  ]
})
export class QuizModule { }

// import { Routes, RouterModule } from '@angular/router';
// import { NgModule, Component } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { UserProfileComponent } from './user-profile/user-profile.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
// import { FormsModule } from '@angular/forms';
// import { UserPhotoComponent } from './user-photo/user-photo.component';
// import { UserImageComponent } from './user-image/user-image.component';
// import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
// import { OnlyLoggedInUsersGuard } from '../guard/only-logged-in-users/only-logged-in-users.guard';

// const routes: Routes = [
//   { path: '', component: DashboardComponent, canActivate: [OnlyLoggedInUsersGuard], },
//   { path: 'profile', component: UserProfileComponent, canActivate: [OnlyLoggedInUsersGuard], },
//   { path: 'photo', component: UserPhotoComponent, canActivate: [OnlyLoggedInUsersGuard], }
// ];

// @NgModule({
//   imports: [
//     CommonModule,
//     FormsModule,
//     RouterModule.forChild(routes),
//   ],
//   declarations: [
//     DashboardComponent,
//     UserProfileComponent,
//     UserPhotoComponent,
//     UserImageComponent,
//     FileDropDirective,
//     FileSelectDirective
//   ]
// })
// export class RegisteredMemberModule { }


// {
//   path: 'test/:course_slug/:topic_slug/:list_slug', // it will fatch data and start test
//     component: TestStartComponent,
//   },
// {
//   path: 'test/:course_slug/:topic_slug',  // it will generate list of selected
//     component: TestListComponent,
//   },
// {
//   path: 'test/:course_slug', // it will generate topic of selected
//     component: TestTopicComponent,
//   },
// {
//   path: 'test',  // it will generate courses avaliable
//     component: TestCourseComponent,
//   },
// {
//   path: 'testresult',  // it will generate courses avaliable
//     component: TestResultComponent,
//   },
