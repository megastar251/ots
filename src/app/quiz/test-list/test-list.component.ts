import { GlobalEmitterService } from './../../service/global-emitter/global-emitter.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {MatCheckboxModule} from '@angular/material';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.css']
})
export class TestListComponent implements OnInit {
  SEOdata: any;
  baseurl: string;
  course_slug: string;
  topic_slug: string;
  data: any;
  showloader: boolean;
  loadingbarChild = true;

  constructor(private myconnection: ApiCommunicateService,
    private myemitter: GlobalEmitterService,
              private route: ActivatedRoute) {
                this.route.params.subscribe( params => {

                  this.course_slug = params['course_slug'];
                  this.topic_slug  = params['topic_slug'];
                });
              }

  ngOnInit() {
    this.showloader = true;
    this.managetestlocation(); // /testportal/{courseid}
    this.myconnection.getData('testportal/' + this.course_slug + '/' + this.topic_slug )
                    .subscribe(result => {

                        this.data = result.data;
                      this.loadingbarChild = false;
                      if (result.seo !== undefined) {
                        this.SEOdata = result.seo;
                        this.myemitter.renderMeta(this.SEOdata);
                      }
                    });
  }



  private managetestlocation() {
    const path = window.location.pathname;
    const subStr = path.substring(0, path.length - 5);
    this.baseurl = subStr;
    return this.baseurl;
    }


    ngOnDestroy(): void {
      // try {
      //   this.myconnection.unsubscribe();
      // } catch (error) {}

      // try {
      //   this.testlist.unsubscribe();
      // } catch (error) {}

      // try {
      //   this.questionlist.unsubscribe();
      // } catch (error) {}

    }

}
