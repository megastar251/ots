
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-test-topic',
  templateUrl: './test-topic.component.html',
  styleUrls: ['./test-topic.component.css']
})
export class TestTopicComponent implements OnInit {
  baseurl: string;
  course_slug: string;
  data: any;
  loadingbarChild = true;

  constructor(private myconnection: ApiCommunicateService,
    private route: ActivatedRoute) {
    this.route.params.subscribe(params => {

      this.course_slug = params['course_slug'];
    });
  }

  ngOnInit() {
    this.managetestlocation(); // /testportal/{courseid}
    this.myconnection.getData('testportal/' + this.course_slug)
      .subscribe(result => {

        this.data = result.data;
        this.loadingbarChild = false;
      });
  }



  private managetestlocation() {
    const path = window.location.pathname;
    const subStr = path.substring(0, path.length - 5);
    this.baseurl = subStr;
    return this.baseurl;
  }

}
