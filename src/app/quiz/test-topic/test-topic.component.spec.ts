import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestTopicComponent } from './test-topic.component';

describe('TestTopicComponent', () => {
  let component: TestTopicComponent;
  let fixture: ComponentFixture<TestTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestTopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
