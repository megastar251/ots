import { ActivatedRoute } from '@angular/router';
import Chart from 'chart.js';
import { Component, OnInit } from '@angular/core';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';
import { UserDataService } from '../../service/user-data/user-data.service';

@Component({
  selector: 'app-test-result',
  templateUrl: './test-result.component.html',
  styleUrls: ['./test-result.component.css']
})
export class TestResultComponent implements OnInit {
  failmessage: string;
  mySubscription: any;
  username: any;
  testkey: any;
  subscription: any;
  // test result variable
  testListdata: any;

  canvas: any;
  ctx: any;

  // score and test info holder
  testscore: any;
  maxtestscore: any;
  testtitle: any;
  quereport: any;
  loadingbarChild = true;

  constructor(private myconnection: ApiCommunicateService,
    private userinfo: UserDataService, private route: ActivatedRoute ) { }

  ngOnInit() {
    this.testscore = 0;
    this.maxtestscore = 0;
    this.testtitle = 0;
    this.username = this.userinfo.getUserName();

    this.route.queryParams
      // .filter(params => params.testkey)
      .subscribe(params => {
        this.testkey = params.testkey;
      });
    this.getTestdetails();
  }

  getTestdetails() {

    this.mySubscription = this.myconnection.postData('testresult',
      { testkey: this.testkey })
      .subscribe(result => {
        if (result.data === undefined) {
          if (result.message === undefined) {
            this.failmessage = 'Failed to Obtain the Report, Please Try to reopen from dashboard';
          }
          this.failmessage = result.message;
        } else {
          if (result.data.length !== 0 ) {
            this.testscore = result.data[0].score;
            this.maxtestscore = result.data[0].max_score;
            this.testtitle = result.data[0].title;
            this.quereport = result.report;
            this.loadingbarChild = false;
          }
        }

      });
  }

  getTestAttemptsList() {
    this.subscription = this.myconnection.getData('testattemptslist')
      .subscribe(result => {
        this.testListdata = result.data;

      });
  }
  ConvertString(value) {
    return parseFloat(value);
  }

  ConvertBoolean(value) {
    if (Number(value) === 0) {
      return false;
    } else if (Number(value) === 1) {
      return true;
    }
    return Number(value);
  }


  // tslint:disable-next-line:use-life-cycle-interface
  // ngAfterViewInit() {
  //   this.canvas = document.getElementById('myChart');
  //   this.ctx = this.canvas.getContext('2d');
  //   const myChart = new Chart(this.ctx, {
  //     type: 'pie',
  //     data: {
  //       labels: ['New', 'In Progress', 'On Hold'],
  //       datasets: [{
  //         label: '# of Votes',
  //         data: [1, 2, 3],
  //         backgroundColor: [
  //           'rgba(255, 99, 132, 1)',
  //           'rgba(54, 162, 235, 1)',
  //           'rgba(255, 206, 86, 1)'
  //         ],
  //         borderWidth: 1
  //       }]
  //     },
  //     options: {
  //       responsive: false,
  //       display: true
  //     }
  //   });
  // }
  seeScoreDetails(id) {

    this.myconnection.postData('testresultdetail', {testid: id})
        .subscribe(result => {

          // this.myScoreDetailTable = result;
          // this.showTestDetailedView = true;
        });
  }

  makechart () {
    const ctx = document.getElementById('myChart');
    const myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    try {
      this.subscription.unsubscribe();
    } catch (error) { }

    try {
      this.mySubscription.unsubscribe();
    } catch (error) { }
  }


}



