import { GlobalEmitterService } from './../../service/global-emitter/global-emitter.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-test-course',
  templateUrl: './test-course.component.html',
  styleUrls: ['./test-course.component.css']
})
export class TestCourseComponent implements OnInit {
  SEOdata: any;
  data: any;
  baseurl: string;
  loadingbarChild = true;

  constructor(private router: Router,
    private myemitter: GlobalEmitterService,
  private myconnection: ApiCommunicateService, ) {  }

  ngOnInit() {
    this.managetestlocation();
        this.myconnection.getData('testportal').subscribe(
          result => {

            this.data = result.data;
            this.loadingbarChild = false;
            if (result.seo !== undefined) {
              this.SEOdata = result.seo;
              this.myemitter.renderMeta(this.SEOdata);
            }
          }
        );
  }

    private managetestlocation() {
        const path = window.location.pathname;
        const subStr = path.substring(0, path.length - 5);
        this.baseurl = subStr;
    }
}
