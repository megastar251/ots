import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCourseComponent } from './test-course.component';

describe('TestCourseComponent', () => {
  let component: TestCourseComponent;
  let fixture: ComponentFixture<TestCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
