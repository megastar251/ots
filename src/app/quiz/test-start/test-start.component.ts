import { GlobalEmitterService } from './../../service/global-emitter/global-emitter.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';

import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {MatCheckboxModule} from '@angular/material';
import { Subscription } from 'rxjs';
import { EventEmitter } from 'events';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';
import { UserDataService } from '../../service/user-data/user-data.service';

@Component({
  selector: 'app-test-start',
  templateUrl: './test-start.component.html',
  styleUrls: ['./test-start.component.css']
})
export class TestStartComponent implements OnInit {
  [x: string]: any;
  testSubmitTrigger: any;
  maxquestiontoattempt: number;
  // browser size
  browserInnerWidth: any;
  browserInnerHeight: any;

  // counter helful
  private $counter: Observable<number>;
  private subscription: Subscription;
  private message: string;
  durationleft: any;
  totalissuedtime: any;
  timeleft: any;
  startcountdown: boolean;
  displaysubmitbtn: boolean;
  showquestionlist: boolean;
  finalsuccessmsg: boolean;

  // supportive variable
  startquiztest: boolean;
  thankyoufortest: boolean;
  testlist: any;
  testduration: any;
  correctscoremarks: any;
  wrongscoremarks: any;
  questioncounter: any;
  fullScreenModeStatus: boolean;

  currentindexposition: any;
  currentid: any;
  currentquestion: any;
  currentopt1: any;
  currentopt2: any;
  currentopt3: any;
  currentopt4: any;

  showquizfirstpage: boolean;
  showquizlastpage: boolean;
  checkedopt1: boolean;
  checkedopt2: boolean;
  checkedopt3: boolean;
  checkedopt4: boolean;

  // request que variable
  questionlist: any;
  testid: string;
  testattempt_id: any;
  showSidequestionNav: boolean;

  baseurl: string;
  course_slug: string;
  topic_slug: string;
  list_slug: string;
  data: any;
  testinstructiondisplay: boolean;
  currentHeightofQuizblock: any;
  currentWidthofQuizblock: any;
  loadingbarChild = true;
  SEOdata: any;

  constructor(
    private myconnection: ApiCommunicateService,
    private userinfo: UserDataService,
    private myemitter: GlobalEmitterService,
    private routerUrl: Router,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {

      this.course_slug = params['course_slug'];
      this.topic_slug = params['topic_slug'];
      this.list_slug = params['list_slug'];
    });

    // this.testSubmitTrigger = new EventEmitter<TestStartComponent>();
  }

  ngOnInit() {
    this.showquestionlist = false;
    this.finalsuccessmsg = false;
    this.thankyoufortest = false;
    this.displaysubmitbtn = false;
    this.testinstructiondisplay = true;
    this.questioncounter = 1;
    this.fullScreenModeStatus = false;
    this.showquizfirstpage = true;
    this.showquizlastpage = false;
    this.showSidequestionNav = false;

    this.managetestlocation(); // /testportal/{courseid}
    this.currentquestion = 'Please Click on question in left pannel to view!';
    this.currentopt1 = '';
    this.currentopt2 = '';
    this.currentopt3 = '';
    this.currentopt4 = '';

    this.checkedopt1 = false;
    this.checkedopt2 = false;
    this.checkedopt3 = false;
    this.checkedopt4 = false;

    this.myconnection
      .getData('testportal/' + this.course_slug + '/' + this.topic_slug + '/' + this.list_slug)
      .subscribe(result => {
        this.totalissuedtime = Number(result.data[0].maximum_duration) * 60 ;
        this.testid = result.data[0].id;
        this.correctscoremarks = Number(result.data[0].correct_score);
        this.wrongscoremarks = Number(result.data[0].wrong_score);
        this.maxquestions = Number(result.data[0].maximum_question);
        this.loadingbarChild = false;

        if (result.seo !== undefined) {
          this.SEOdata = result.seo;
          this.myemitter.renderMeta(this.SEOdata);
        }
      });
    this.browserInnerWidth = window.innerWidth;
    if (this.browserInnerWidth < 480) {
      this.showSidequestionNav = true;
    }
    this.myemitter.renderTitle('Welcome to Practice Section of OTS');
  }

  starttestform(f) {
    this.loadingbarChild = true;
    this.startquiztest = true;
    this.displaysubmitbtn = true;
    this.showquestionlist = true;
    this.myconnection
      .postData('testportal/' + this.course_slug + '/' + this.topic_slug + '/' + this.list_slug, '')
      .subscribe(result => {

        this.testinstructiondisplay = false;
        this.questionlist = result.data;
        this.testattempt_id = result.testattempt_id;
        this.loadingbarChild = false;
        this.startTest();
      });
  }

  startTest() {
    // this.totalissuedtime = 5;

    // this.totalissuedtime = parseInt(this.testduration);
    this.startcountdown = true;
    if (this.startcountdown === true) {
      this.$counter = Observable.interval(1000).map(x => {
        this.durationleft = this.totalissuedtime - x;
        this.timeleft = this.MyTimeCounter(this.durationleft);
        this.myemitter.renderTitle(this.timeleft + 'Time to end');
        if (this.durationleft === 0) {

          this.startcountdown = false;
          this.showquestionlist = false;
          this.ngOnDestroy();
          this.thankyoufortest = true;
          // this.router.navigate(['/dashboard']);
          // this.nativeElement.getElementById('timmerbox').setAttribute("class", "ad-red");
        }
        return x;
      });
    }

    this.subscription = this.$counter.subscribe(x => {
      if (this.startcountdown === true) {
        this.message = this.MyTimeCounter(this.durationleft);
      } else {
      }
    });
  }

  submitTestForm(f) {
    this.startcountdown = false;
    this.showquestionlist = false;
    this.thankyoufortest = false;
    this.displaysubmitbtn = false;
    this.finalsuccessmsg = true;
    this.ngOnDestroy();


    const data = f.value;
    this.myconnection
      .postData('testsubmit', {
        testid: this.testid,
        correctscoremarks: this.correctscoremarks,
        wrongscoremarks: this.wrongscoremarks,
        studentid: this.userinfo.getEmail(),
        testattempt_id: this.testattempt_id,
        quevsopt: data
      })
      .subscribe(result => {
        this.routerUrl.navigate(['/members']);
      });
  }

  private managetestlocation() {
    const path = window.location.pathname;
    const subStr = path.substring(0, path.length - 5);
    this.baseurl = subStr;


    return this.baseurl;
  }

  MyTimeCounter(t) {
    // t is in seconds
    let hours, minutes, seconds;
    hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;
    minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    seconds = t % 60;

    return [hours + 'h', minutes + 'm', seconds + 's'].join(' ');
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    this.myemitter.renderTitle('Thank You for Using Practice Section of OTS');
    try {
      this.subscription.unsubscribe();
    } catch (error) {}
  }

  /*
  *---------------------------------------------------------------------------
  * Helper functions
  *---------------------------------------------------------------------------
  */

  // ********** button color change ***********
  questionNoClicked(event, targetform, indexposition) {
    this.displayQuestion(event, targetform, indexposition);
  }
  buttonMakeAttempted(indexposition) {
    const i = document.getElementById('button' + indexposition);
    i.setAttribute('style', 'background-color:green');
  }
  buttonMakeMarked(indexposition) {
    const i = document.getElementById('button' + indexposition);
    i.setAttribute('style', 'background-color:yellow');
  }
  buttonMakeViewed(indexposition) {
    const i = document.getElementById('button' + indexposition);

    if (i.getAttribute('style') === null) {
      i.setAttribute('style', 'background-color:#7FB3D5');
    }
  }
  buttonMakeClear(indexposition) {
    const i = document.getElementById('button' + indexposition);
    i.setAttribute('style', null);
  }

  // ######### button color change #########

  fullScreen() {
    this.fullScreenModeStatus = !this.fullScreenModeStatus;
    const i = document.getElementById('quizsection');
    // const i = document.getElementById('quizblock');
    if (this.fullScreenModeStatus) {
      this.currentHeightofQuizblock = i.getAttribute('height');
      this.currentWidthofQuizblock = i.getAttribute('width');
      i.setAttribute('width', window.innerWidth + 'px');
      i.setAttribute('height', '500px');
      // go full-screen
      if (i.requestFullscreen) {
        i.requestFullscreen();
      } else if (i.webkitRequestFullscreen) {
        i.webkitRequestFullscreen();
      }
    } else {
      i.setAttribute('height', this.currentHeightofQuizblock + 'px');
      i.setAttribute('height', this.currentWidthofQuizblock + 'px');
      // exit full-screen
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }
  }

  optionSelected(event, q) {

    const i = document.getElementById('abc123');
    i.setAttribute('class', 'btn btn-success');
  }

  // display the question
  displayQuestion(event, targetform, indexposition) {
    if (this.showquizfirstpage === true) {
      this.showquizfirstpage = false;
    }
    this.currentindexposition = indexposition;
    this.currentid =        this.questionlist[indexposition]['id'];
    this.currentquestion =  this.questionlist[indexposition]['question'];
    this.currentopt1 =      this.questionlist[indexposition]['opt1'];
    this.currentopt2 =      this.questionlist[indexposition]['opt2'];
    this.currentopt3 =      this.questionlist[indexposition]['opt3'];
    this.currentopt4 =      this.questionlist[indexposition]['opt4'];

    this.checkedopt1 = false;
    this.checkedopt2 = false;
    this.checkedopt3 = false;
    this.checkedopt4 = false;

    if (targetform.value[this.currentid] === 1) {
      this.checkedopt1 = true;
    } else if (targetform.value[this.currentid] === 2) {
      this.checkedopt2 = true;
    } else if (targetform.value[this.currentid] === 3) {
      this.checkedopt3 = true;
    } else if (targetform.value[this.currentid] === 4) {
      this.checkedopt4 = true;
    } else {
      // this.checkedopt4 = 'checked';
    }



    this.buttonMakeViewed(indexposition);
    this.closeNav();
  }

  makeCurrentButtonSelected(value) {
    this.checkedopt1 = false;
    this.checkedopt2 = false;
    this.checkedopt3 = false;
    this.checkedopt4 = false;

    if (value === 1) {
      this.checkedopt1 = true;
    } else if (value === 2) {
      this.checkedopt2 = true;
    } else if (value === 3) {
      this.checkedopt3 = true;
    } else if (value === 4) {
      this.checkedopt4 = true;
    } else {
      // this.checkedopt4 = 'checked';
    }
  }


  assignThevalueOnClick(targetform, questionID, value) {
    targetform.value[questionID] = value;

    this.buttonMakeAttempted(this.currentindexposition);
    this.makeCurrentButtonSelected(value);
  }

  openNav() {
    if (this.showSidequestionNav) {
      const i = document.getElementById('style-2');
      i.setAttribute('style', 'width:250px');

    }
  }
  closeNav() {
    if (this.showSidequestionNav) {
      const i = document.getElementById('style-2');
      i.setAttribute('style', 'width:0px');

    }
  }
  switchNavQuestion() {
    if (this.showSidequestionNav) {
      const i = document.getElementById('style-2');
      i.setAttribute('style', 'width:0px');

    }
  }




  markForReviewOption(event, targetform, indexposition) {
    this.buttonMakeMarked(indexposition);
    const maxlength = this.questionlist.length;
    if (indexposition < maxlength) {
      indexposition = indexposition + 1;
      this.displayQuestion(event, targetform, indexposition);
    }
  }
  clearMarkedOption(event, targetform, indexposition) {
    targetform.value[this.currentid] = '';
    this.buttonMakeClear(indexposition);
  }
  saveAndNextOption(event, targetform, indexposition) {
    const maxlength = this.questionlist.length;
    if (indexposition < maxlength) {
      indexposition = indexposition + 1;
      this.displayQuestion(event, targetform, indexposition);
    }
  }
}



