import { CookieService } from 'ngx-cookie-service';
import { UserDataService } from './../service/user-data/user-data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private cookieService: CookieService ,
    private userinfo: UserDataService, ) { }

  ngOnInit() {
    this.clearAll();
  }

  clearAll() {
    this.cookieService.deleteAll();
    this.userinfo.clearAll();
  }

}
