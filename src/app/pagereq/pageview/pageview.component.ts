import { GlobalEmitterService } from './../../service/global-emitter/global-emitter.service';
import { ActivatedRoute } from '@angular/router';

import { Component, OnInit } from '@angular/core';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-pageview',
  templateUrl: './pageview.component.html',
  styleUrls: ['./pageview.component.css']
})
export class PageviewComponent implements OnInit {
  SEOdata: any;
  pagedisplaydata: any;
  subscription: any;
  pageview_slug: any;
  pagesubcategory_slug: any;
  page_category_slug: any;
  loadingbarChild = true;

  constructor(
    private myconnection: ApiCommunicateService,
    private myemitter: GlobalEmitterService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {

      // page_category_slug
      this.page_category_slug = params['page_category_slug'];
      this.pagesubcategory_slug = params['pagesubcategory_slug'];
      this.pageview_slug = params['pageview_slug'];
    });
  }

  ngOnInit() {
    this.getPageDisplayData();
  }

  getPageDisplayData() {
    this.subscription = this.myconnection
      .getData(
        'pages/' +
          this.page_category_slug +
          '/' +
          this.pagesubcategory_slug + '/' +
          this.pageview_slug
      )
      .subscribe(result => {
        this.pagedisplaydata = result.data;
        this.loadingbarChild = false;
        if (result.seo !== undefined) {
          this.SEOdata = result.seo;
          this.myemitter.renderMeta(this.SEOdata);
        }
      });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    try {
      this.subscription.unsubscribe();
    } catch (error) {}
  }
}
