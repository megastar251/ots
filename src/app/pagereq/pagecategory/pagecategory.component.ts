import { GlobalEmitterService } from '../../service/global-emitter/global-emitter.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-pagecategory',
  templateUrl: './pagecategory.component.html',
  styleUrls: ['./pagecategory.component.css']
})
export class PagecategoryComponent implements OnInit {
  SEOdata: any;
  getpagecategory: any;
  subscription: any;
  page_category_slug: any;

  pagecategorytitle: any;
  pagecategorydesc: any;
  pagecategorydata: any;
  pagesidecategorydata: any;
  pagesidedirectlink: any;
  loadingbarChild = true;

  constructor(private myconnection: ApiCommunicateService,
    private myemitter: GlobalEmitterService,
              private route: ActivatedRoute) {
    this.route.params.subscribe( params => {

      // page_category_slug
      this.page_category_slug = params['page_category_slug'];
    });
  }

  ngOnInit() {
    this.makecategorypage();
    this.getpagecategorylist();
  }

  makecategorypage(): any {
    this.pagecategorytitle = 'Syllabus';
    this.pagecategorydesc = 'An example for syllabus for many examination';
    this.pagesidecategorydata = 'n';
    this.pagesidedirectlink = 'aaaa';
  }

  getpagecategorylist() {
    this.subscription = this.myconnection.getData('pages/' + this.page_category_slug )
    .subscribe(result => {
        this.getpagecategory = result.data;
      this.loadingbarChild = false;
      if (result.seo !== undefined) {
            this.SEOdata = result.seo;
            this.myemitter.renderMeta(this.SEOdata);
          }
    });
  }


  ngOnDestroy() {
    try {
      this.subscription.unsubscribe();
    } catch (error) { }

  }

}
