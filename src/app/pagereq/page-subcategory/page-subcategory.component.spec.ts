import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSubcategoryComponent } from './page-subcategory.component';

describe('PageSubcategoryComponent', () => {
  let component: PageSubcategoryComponent;
  let fixture: ComponentFixture<PageSubcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSubcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSubcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
