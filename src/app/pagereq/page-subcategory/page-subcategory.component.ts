import { GlobalEmitterService } from '../../service/global-emitter/global-emitter.service';
import { ActivatedRoute } from '@angular/router';

import { Component, OnInit } from '@angular/core';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-page-subcategory',
  templateUrl: './page-subcategory.component.html',
  styleUrls: ['./page-subcategory.component.css']
})
export class PageSubcategoryComponent implements OnInit {
  subscription: any;
  pagesubcategory_slug: any;
  page_category_slug: any;
  pagesubcategorydata: any;
  loadingbarChild = true;

  SEOdata: any;

  constructor(private myconnection: ApiCommunicateService,
    private myemitter: GlobalEmitterService,
    private route: ActivatedRoute) {
    // get slugs
    this.route.params.subscribe(params => {
      this.page_category_slug = params['page_category_slug'];
      this.pagesubcategory_slug = params['pagesubcategory_slug'];
    });

  }

  ngOnInit() {
    this.getpagesubcategorylist();
  }


  getpagesubcategorylist() {
    this.subscription = this.myconnection.getData('pages/' + this.page_category_slug + '/' + this.pagesubcategory_slug)
      .subscribe(result => {
        if (result.data !== undefined) {
          this.pagesubcategorydata = result.data;
          this.loadingbarChild = false;
          if (result.seo !== undefined) {
            this.SEOdata = result.seo;
            this.myemitter.renderMeta(this.SEOdata);
          }
        }
      });
  }



  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    try {
      this.subscription.unsubscribe();
    } catch (error) { }

  }

}
