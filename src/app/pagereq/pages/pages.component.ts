import { GlobalEmitterService } from './../../service/global-emitter/global-emitter.service';

import { Component, OnInit } from '@angular/core';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {
  SEOdata: any;
  subscription: any;
  array = [10, 20, 30, 50, 10, 20, 25, 45, 40];
  pagecat: any;
  pagescount: any;
  loadingbarChild = true;

  constructor(private myconnection: ApiCommunicateService,
    private myemitter: GlobalEmitterService, ) {}
  // getImportantData;
  ngOnInit() {
    this.getPagesName();

  }

  getPagesName() {
    this.subscription = this.myconnection
      .getData('pages')
      .subscribe(result => {
        this.pagecat = result.data;
        this.loadingbarChild = false;
        if (result.seo !== undefined) {
          this.SEOdata = result.seo;
          this.myemitter.renderMeta(this.SEOdata);
        }
      });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    try {
      this.subscription.unsubscribe();
    } catch (error) {}
  }
}
