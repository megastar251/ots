import { SharedModuleModule } from './../shared-module/shared-module.module';
import { PageviewComponent } from './pageview/pageview.component';
import { PageSubcategoryComponent } from './page-subcategory/page-subcategory.component';
import { PagecategoryComponent } from './pagecategory/pagecategory.component';
import { PagesComponent } from './pages/pages.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: ':page_category_slug/:pagesubcategory_slug/:pageview_slug', // it will fatch data and start test
    component: PageviewComponent,
  },
  {
    path: ':page_category_slug/:pagesubcategory_slug',  // it will generate list of selected
    component: PageSubcategoryComponent,
  },
  {
    path: ':page_category_slug', // it will generate topic of selected
    component: PagecategoryComponent,
  },
  {
    path: '', // it will generate topic of selected
    component: PagesComponent,
  },
];


@NgModule({
  imports: [
    CommonModule,
    SharedModuleModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    PageviewComponent,
    PageSubcategoryComponent,
    PagecategoryComponent,
    PagesComponent,
  ]
})
export class PagereqModule { }
