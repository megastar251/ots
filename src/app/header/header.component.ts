import { UserDataService } from './../service/user-data/user-data.service';
import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showNavBarUserName: any;
  @Input() loggedUserName: any;
  @Input() logedStatus: any;
  @Input() alertMessage: any;
  @Input() alertMessageStatus: any;

  myMessage: String;
  loginStatus: boolean;
  toggleMenu: boolean;
  messageAreaShow: boolean;
  @Input() loginASUserName: String;

  constructor(private userdata: UserDataService) {
  }

  ngOnInit() {
    this.toggleMenu = false;
    this.messageAreaShow = true;
    this.loginStatus = true;
    this.myMessage = this.userdata.getheaderMenu();
    this.loggedUserName = this.userdata.getUserName().replace('"', '').replace('"', '');
    this.logedStatus = this.userdata.getLoggedInStatus();
    this.getAll();
    // this.loginASUser = "Avdesh";
    // this.loginASUserName = this.userdata.getUserName();
  }

  getAll() {
    this.userdata.showNavBarUserName.subscribe((mode: any) => {
      this.showNavBarUserName = mode;
    });

    this.userdata.EmittedAlertMessage.subscribe((mode: any) => {
      this.alertMessage = mode;
    });

    this.userdata.EmittedAlertMessage.subscribe((mode: any) => {
      this.alertMessageStatus = mode;
    });

    this.userdata.EmittedLogedInStatus.subscribe((mode: any) => {
      this.logedStatus = mode;
    });

    this.userdata.EmittedLogedInUserName.subscribe((mode: any) => {
      this.loggedUserName = mode;
    });
  }
  // ngOnChanges() {
  //   this.titleChanged.emit(this.loginStatus);
  // }

  mobileResponsiveNavBar(event: MouseEvent, tMenu: any) {
    if (this.toggleMenu === false) {
      tMenu.setAttribute('class', 'expand navbar-collapse');
      this.toggleMenu = true;
    } else {
      tMenu.setAttribute('class', 'collapse navbar-collapse');
      this.toggleMenu = false;
    }
  }

  closeNavMenu(event: MouseEvent, tMenu: any) {
    tMenu.setAttribute('class', 'collapse navbar-collapse');
    this.toggleMenu = false;
  }

  messageAreaShowToggle() {
    this.messageAreaShow = !this.messageAreaShow;
  }
}

