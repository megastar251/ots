import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { UserPhotoComponent } from './user-photo/user-photo.component';
// import { FileSelectDirective } from 'ng2-file-upload';
import { OnlyLoggedInUsersGuard } from '../guard/only-logged-in-users/only-logged-in-users.guard';

const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [OnlyLoggedInUsersGuard], },
  { path: 'profile', component: UserProfileComponent, canActivate: [OnlyLoggedInUsersGuard], },
  { path: 'photo', component: UserPhotoComponent, canActivate: [OnlyLoggedInUsersGuard], }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    UserPhotoComponent,
    // FileDropDirective,
    // FileSelectDirective
  ]
})
export class RegisteredMemberModule { }
