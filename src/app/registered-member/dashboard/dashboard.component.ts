import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../../service/user-data/user-data.service';
import { DatePipe } from '@angular/common';
import { ApiCommunicateService } from '../../service/api-communicate/api-communicate.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  showTestAttemptedList: boolean;
  showTestAvaliableList: boolean;
  attempteddata: any;
  avaliabledata: any;
  username: any;

  constructor(private myconnection: ApiCommunicateService,
    private userinfo: UserDataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.showTestAttemptedList = true;
    this.showTestAvaliableList = false;

    this.getAttemptedTestData();
    this.getAvaliableTestData();
    this.username = this.userinfo.getUserName();
  }
  switchtab(id) {
    this.switchBetweenForms(id);
  }

  switchBetweenForms(tabid) {
    // disabling all forms
    this.showTestAttemptedList = false;
    this.showTestAvaliableList = false;

    const attemptedTab = document.getElementById('attemptedTab');
    const avaliableTab = document.getElementById('avaliableTab');

    attemptedTab.setAttribute('class', 'nav-link');
    avaliableTab.setAttribute('class', 'nav-link');

    switch (tabid) {
      case 1:
        attemptedTab.setAttribute('class', 'nav-link active');
        this.showTestAttemptedList = true;
        break;
      case 2:
        avaliableTab.setAttribute('class', 'nav-link active');
        this.showTestAvaliableList = true;
        break;
      default:
        break;
    }
  }

  // Now getting data
  getAttemptedTestData() {
    this.myconnection
      .getData('testattemptslist')
      .subscribe(result => {
        this.attempteddata = result.data;
      });
  }
  getAvaliableTestData() {
    this.myconnection
      .getData('testavaliablelist')
      .subscribe(result => {
        this.avaliabledata = result.data;
      });
  }

  gotoResults() {
    // this.route.navigate(['/test/result'], { queryParams: { order: 'popular' } });
  }
}
