import { UserDataService } from './../../service/user-data/user-data.service';
import { ApiCommunicateService } from './../../service/api-communicate/api-communicate.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  username: string;
  showSkillForm: boolean;
  showEducationForm: boolean;
  showPersonalDetailForm: boolean;
  showInterestForm: boolean;
  myvar: any;

  skilldata: any;
  educationdata: any;
  personaldata: any;
  interestdata: any;
  referraldata: any;

  constructor(private myconnection: ApiCommunicateService,
    private userinfo: UserDataService ) { }

  ngOnInit() {
    this.showSkillForm = true;
    this.getskilldata();
    this.username = this.userinfo.getUserName();
  }

  switchBetweenForms(tabid) {
    // disabling all forms
    this.showSkillForm = false;
    this.showEducationForm = false;
    this.showPersonalDetailForm = false;
    this.showInterestForm = false;

    const skillstab = document.getElementById('skillstab');
    const educationtab = document.getElementById('educationtab');
    const personaltab = document.getElementById('personaltab');
    const interesttab = document.getElementById('interesttab');

    skillstab.setAttribute('class', 'nav-link');
    educationtab.setAttribute('class', 'nav-link');
    personaltab.setAttribute('class', 'nav-link');
    interesttab.setAttribute('class', 'nav-link');

    switch (tabid) {
      case 1:
        skillstab.setAttribute('class', 'nav-link active');
        this.showSkillForm = true;
        this.getskilldata();
        break;
      case 2:
        educationtab.setAttribute('class', 'nav-link active');
        this.showEducationForm = true;
        this.geteducationdata();
        break;
      case 3:
        personaltab.setAttribute('class', 'nav-link active');
        this.showPersonalDetailForm = true;
        this.getpersonaldata();
        break;
      case 4:
        interesttab.setAttribute('class', 'nav-link active');
        this.showInterestForm = true;
        this.getinterestdata();
        break;
      default:
        this.showSkillForm = true;
        break;
    }
  }

  switchtab(id) {
    this.switchBetweenForms(id);
  }

  alertMe(): void {
    setTimeout(function (): void {
      alert('You\'ve selected the alert tab!');
    });
  }







  // Now getting data
  getskilldata() {
    this.myconnection
      .getData('skill')
      .subscribe(result => {
        this.skilldata = result.data;
      });
  }


  geteducationdata() {
    this.myconnection
      .getData('education')
      .subscribe(result => {
        this.educationdata = result.data;
      });
  }

  getpersonaldata() {
    this.myconnection
      .getData('personal')
      .subscribe(result => {
        this.personaldata = result.data;
      });
  }

  getinterestdata() {
    this.myconnection
      .getData('interest')
      .subscribe(result => {
        this.interestdata = result.data;
      });
  }

  getreferraldata() {
    this.myconnection
      .getData('referral')
      .subscribe(result => {
        this.referraldata = result.data;
      });
  }






  // Now submiting forms
  skillFormSubmit(myform) {

    const formdata = myform.value;
    this.myconnection
      .postData('skill', {
        user: this.userinfo.getUserName(),
        data: formdata,
      })
      .subscribe(result => {
        this.skilldata = result.data;
      });
  }

  educationFormSubmit(myform) {

    const formdata = myform.value;
    this.myconnection
      .postData('education', {
        user: this.userinfo.getUserName(),
        data: formdata,
      })
      .subscribe(result => {
        this.educationdata = result.data;
      });
  }

  personalFormSubmit(myform) {
    //
    const formdata = myform.value;
    this.myconnection
      .postData('personal', {
        user: this.userinfo.getUserName(),
        data: formdata,
      })
      .subscribe(result => {
        this.personaldata = result.data;
      });
  }

  interestFormSubmit(myform) {
    //
    const formdata = myform.value;
    this.myconnection
      .postData('interest', {
        user: this.userinfo.getUserName(),
        data: formdata,
      })
      .subscribe(result => {
        this.interestdata = result.data;
      });
  }
}
