import { LogoutComponent } from './logout/logout.component';
// import { RegisteredMemberModule } from './registered-member/registered-member.module';
import { LinkNotFoundComponent } from './link-not-found/link-not-found.component';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AppComponent } from './app.component';


import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'activity', // it will fatch data and start test
    loadChildren: './activity/activity.module#ActivityModule',
  },
  {
    path: 'user/forms',
    component: AppComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'logout',
    component: LogoutComponent,
  },
  {
    path: 'pages',
    loadChildren: './pagereq/pagereq.module#PagereqModule',
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'test', // it will fatch data and start test
    loadChildren: './quiz/quiz.module#QuizModule',
  },
  {
    path: 'members',  // it will generate courses avaliable
    loadChildren: './registered-member/registered-member.module#RegisteredMemberModule',
  },
  {
    path: '**',  // it will generate courses avaliable
    component: LinkNotFoundComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
