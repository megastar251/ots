import { UserDataService } from './../service/user-data/user-data.service';
import { ApiCommunicateService } from './../service/api-communicate/api-communicate.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  redirectedfrom: any;
  mySubscription: any;
  private data: string;
  check: any;
  url: any;
  abc: any;
  token: any;
  loginemail: string;
  loginPassword: string;
  checkinguser: any;
  failmessage: any;
  showToastNotif: boolean;


  constructor(private router: Router,
              private route: ActivatedRoute ,
              private userinfo: UserDataService,
              private myconnection: ApiCommunicateService) { }

  ngOnInit() {
      this.check = this.userinfo.getLoggedInStatus();
      if (this.check === true) {
        this.router.navigate(['/members']);
      }
      this.checkinguser = false;
      this.showToastNotif = false;

    this.route.queryParams
      // .filter(params => params.testkey)
      .subscribe(params => {
        this.redirectedfrom = params.redirectedfrom;
        if (this.redirectedfrom !== undefined) {
          alert('Please Register or Login to continue...');
        }
      });
  }

  submitLogin(formData) {
    this.checkinguser = true;
    this.loginemail = formData.value.loginemail;
    this.loginPassword = formData.value.userPassword;
    this.showToastNotif = true;
    this.showToast();

    if (formData.valid) {
      this.mySubscription = this.myconnection.postData('login',
          {email: this.loginemail.toString(), password: this.loginPassword.toString() })
      .subscribe(result => {
        if (result.success === undefined) {
          if (result.message === undefined) {
            this.failmessage = 'Failed to login, Please input correct Email and Password';
          }
          this.failmessage = result.message;
        } else {
          this.userinfo.setUserToken(result.success.token);
          this.userinfo.setEmail(this.loginemail.toString());
          this.userinfo.setUserName(result.success.name);
            if (this.redirectedfrom === undefined) {
              this.router.navigate(['/members']);
            } else {
              // this.router.navigateByUrl(this.redirectedfrom);
              window.location.replace(this.redirectedfrom);
            }
        }

        });
    }
  }

  ngOnDestroy() {
    try {
      this.mySubscription.unsubscribe();
    } catch (error) {   }
  }

  showToast() {
    //
    this.showToastNotif = true;
    const toast = document.getElementById('snackbar');

    this.showLoginToast();
    if (!this.showToastNotif) {
      toast.setAttribute('style', 'visibility:hidden');
    }
  }

  showLoginToast() {

    const toast = document.getElementById('snackbar');
    if (this.showToastNotif) {
      toast.setAttribute('class', 'show');
    }

    setTimeout(() => {
      this.showToastNotif = false;

      toast.setAttribute('class', '');
    }, 3000);
  }
}
