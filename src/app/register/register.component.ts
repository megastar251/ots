import { UserDataService } from './../service/user-data/user-data.service';
import { ApiCommunicateService } from './../service/api-communicate/api-communicate.service';

import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  redirectedfrom: any;
  failmessage: string;
  upassword2: any;
  url: string;
  abc: any;
  token: any;
  uname: string;
  uemail: string;
  upassword: string;
  showToastNotif: boolean;

  constructor(private router: Router, private route: ActivatedRoute,
              private myconnection: ApiCommunicateService,
              private userinfo: UserDataService) { }

  ngOnInit() {
    this.showToastNotif = false;

    this.route.queryParams
      // .filter(params => params.testkey)
      .subscribe(params => {
        this.redirectedfrom = params.redirectedfrom;
        console.log(this.redirectedfrom);
      });
  }

  submit(f) {

    this.uname = f.value.userName;
    this.uemail = f.value.userEmail1;
    this.upassword = f.value.userPassword1;
    this.upassword2 = f.value.userPassword2;

    this.showToastNotif = true;
    this.showToast();

    if (f.valid) {
      this.myconnection.postData('register', { name: this.uname, email: this.uemail, password: this.upassword, c_password: this.upassword2})
      .subscribe(result => {
        if (result.success === undefined) {
          if (result.message === undefined) {
            this.failmessage = 'Failed to register, Please input correct Email, Password and confirmpassword';
          }
          this.failmessage = result.message;
        } else {
          this.userinfo.setUserToken(result.success.token);
          this.userinfo.setEmail(this.uemail.toString());
          this.userinfo.setUserName(result.success.name);

          if (this.redirectedfrom === undefined) {
            this.router.navigate(['/members']);
          } else {
            // this.router.navigateByUrl(this.redirectedfrom);
            window.location.replace(this.redirectedfrom);
          }
        }
        });
    }
  }


  showToast() {
    //
    this.showToastNotif = true;
    const toast = document.getElementById('snackbar');

    this.showLoginToast();
    if (!this.showToastNotif) {
      toast.setAttribute('style', 'visibility:hidden');
    }
  }

  showLoginToast() {

    const toast = document.getElementById('snackbar');
    if (this.showToastNotif) {
      toast.setAttribute('class', 'show');
    }
    setTimeout(() => {
      this.showToastNotif = false;

      toast.setAttribute('class', '');
    }, 3000);
  }


}
