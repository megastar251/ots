import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserDataService } from '../../service/user-data/user-data.service';

@Injectable()
export class OnlyLoggedInUsersGuard implements CanActivate {
  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //   return true;
  // }

  private permitt: boolean;

  constructor(private userinfo: UserDataService,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // return true;
    this.permitt = (this.userinfo.getLoggedInStatus()) ? this.userinfo.getLoggedInStatus() : false;
    if (this.permitt === false) {
      const redirectto = window.location.href ;
      console.log(redirectto);
      this.router.navigate(['/login'], { queryParams: { redirectedfrom: redirectto } });
    }
    return this.permitt;
  }
}
