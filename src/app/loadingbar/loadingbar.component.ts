import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loadingbar',
  templateUrl: './loadingbar.component.html',
  styleUrls: ['./loadingbar.component.css']
})
export class LoadingbarComponent implements OnInit {
  @Input() status: boolean;
  constructor() { }

  ngOnInit() {
  }
}
